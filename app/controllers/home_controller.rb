class HomeController < ApplicationController
  respond_to :html, :json

  def home
  	@datacenters=Datacenter.all
	respond_with @datacenters do |format|
	  format.html
	  format.json {render json: @datacenters}
	end
  end

  def about
  end
end
