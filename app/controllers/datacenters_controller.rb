class DatacentersController < ApplicationController
	respond_to :html, :json

	def index
		@datacenters=Datacenter.all
		respond_to do |format|
		  format.html
		  format.js {render json: @datacenters}
		end
	end
end
