class Datacenter < ActiveRecord::Base
  attr_accessible :dcid,:activeCadUsers, :activeVisUsers, :cadApplyFilter, :cadConnect, :cadDsearch, :cadExplore, :cadOpen, :cadOperationsExcuation, :cadSearch, :cadSelect, :cadUpdate, :cadUsers, :cadValidate, :country, :fcsCpu, :hour, :minute, :visApplyFilter, :visConnect, :visDSearch, :visExplore, :visOpen, :visOperationsExcuation, :visSearch, :visSelect, :visUpdate, :visUsers, :visValidate
  has_and_belongs_to_many :countries
end
