class VisApp < ActiveRecord::Base
  attr_accessible :activeUsers, :applyFiler, :connect, :explore, :logged_user, :open, :operationsInExecution, :search, :select, :tdSearch, :update, :validate
  belongs_to :datacenter
end
