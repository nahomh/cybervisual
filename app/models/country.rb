class Country < ActiveRecord::Base
  attr_accessible :country
  has_and_belongs_to_many :datacenters
end
