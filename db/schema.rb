# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130806013010) do

  create_table "countries", :force => true do |t|
    t.string   "country"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "datacenters", :force => true do |t|
    t.string   "country"
    t.float    "hour"
    t.float    "minute"
    t.float    "cadUsers"
    t.float    "cadOperationsExcuation"
    t.float    "activeCadUsers"
    t.float    "fcsCpu"
    t.float    "cadConnect"
    t.float    "cadSearch"
    t.float    "cadExplore"
    t.float    "cadApplyFilter"
    t.float    "cadDsearch"
    t.float    "cadSelect"
    t.float    "cadValidate"
    t.float    "cadOpen"
    t.float    "cadUpdate"
    t.float    "visUsers"
    t.float    "visOperationsExcuation"
    t.float    "activeVisUsers"
    t.float    "visConnect"
    t.float    "visSearch"
    t.float    "visExplore"
    t.float    "visApplyFilter"
    t.float    "visDSearch"
    t.float    "visSelect"
    t.float    "visValidate"
    t.float    "visOpen"
    t.float    "visUpdate"
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
    t.integer  "dcid"
  end

end
