class CountryDatacenter < ActiveRecord::Migration
  def up
    create_table 'countries_datacenters', :id => false do |t|
      t.column :country_id, :integer
      t.column :datacenter_id, :integer
    end
  end

  def down
  end
end
