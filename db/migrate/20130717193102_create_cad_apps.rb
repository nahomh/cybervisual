class CreateCadApps < ActiveRecord::Migration
  def change
    create_table :cad_apps do |t|
      t.float :logged_user
      t.float :operationsInExecution
      t.float :activeUsers
      t.float :connect
      t.float :search
      t.float :explore
      t.float :applyFiler
      t.float :tdSearch
      t.float :select
      t.float :validate
      t.float :open
      t.float :update

      t.timestamps
    end
  end
end
