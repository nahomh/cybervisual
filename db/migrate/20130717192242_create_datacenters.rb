class CreateDatacenters < ActiveRecord::Migration
  def change
    create_table :datacenters do |t|
      t.integer :minute
      t.integer :hour
      t.float :fcs_cpu

      t.timestamps
    end
  end
end
