class AddDcidToDatacenters < ActiveRecord::Migration
  def change
    add_column :datacenters, :dcid, :integer
  end
end
