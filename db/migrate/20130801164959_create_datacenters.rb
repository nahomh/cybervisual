class CreateDatacenters < ActiveRecord::Migration
  def change
    create_table :datacenters do |t|
      t.string :country
      t.float :hour
      t.float :minute
      t.float :cadUsers
      t.float :cadOperationsExcuation
      t.float :activeCadUsers
      t.float :fcsCpu
      t.float :cadConnect
      t.float :cadSearch
      t.float :cadExplore
      t.float :cadApplyFilter
      t.float :cadDsearch
      t.float :cadSelect
      t.float :cadValidate
      t.float :cadOpen
      t.float :cadUpdate
      t.float :visUsers
      t.float :visOperationsExcuation
      t.float :activeVisUsers
      t.float :visConnect
      t.float :visSearch
      t.float :visExplore
      t.float :visApplyFilter
      t.float :visDSearch
      t.float :visSelect
      t.float :visValidate
      t.float :visOpen
      t.float :visUpdate

      t.timestamps
    end
  end
end
