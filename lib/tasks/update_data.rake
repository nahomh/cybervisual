require 'csv'

desc 'Load simulation data'
task :load_simulation_data => [:load_countries, :load_datacenters] do
end

desc 'Load countries'
task :load_countries => :environment do
  require 'csv'

  csv = CSV.open('db/countries.csv', :headers => true)
  [:convert, :header_convert].each { |c| csv.send(c) { |f| f.strip } }
  csv.each do |row|
    Country.find_or_create_by_country(row[0])    
  end
end
desc 'Load datacenters'
task :load_datacenters => [:environment] do
	Datacenter.delete_all
	CSV.foreach('db/simData2.csv', :headers => true) do |row|
		Datacenter.create!(:dcid => row[0],
			:country => row[1],
    		:minute => row[2],
    		:hour => row[3],
			:cadUsers => row[4],
    		:cadOperationsExcuation => row[5],    		
			:activeCadUsers => row[6],
    		:fcsCpu => row[7],
			:cadConnect => row[8],
    		:cadSearch => row[9],
			:cadExplore => row[10],
    		:cadApplyFilter => row[11],
			:cadDsearch => row[12],
    		:cadSelect => row[13],    		
			:cadValidate => row[14],
    		:cadOpen => row[15],
			:cadUpdate => row[16],
    		:visUsers => row[17],
			:visOperationsExcuation => row[18],
    		:activeVisUsers => row[19],
			:visConnect => row[20],
    		:visSearch => row[21],
			:visExplore => row[22],
    		:visApplyFilter => row[23],    		
			:visDSearch => row[24],
    		:visSelect => row[25],
			:visValidate => row[26],
    		:visOpen => row[27],
			:visUpdate => row[28]
		)
	end
end

